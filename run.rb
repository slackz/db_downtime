require 'mysql2'

# seconds to sleep between queries to the db
SLEEP_INTERVAL = 1
CLIENT = Mysql2::Client.new(default_file: './.my.cnf',
                            default_group: 'client',
                            reconnect: true)
@downtime = 0

def db_up?
  begin
    CLIENT.query('SELECT NOW()').inspect
  rescue Mysql2::Error
    @downtime += SLEEP_INTERVAL
    return false
  end

  return true
end

while(true)
  res = db_up?
  msg = "db is #{res ? 'up' : 'down'} - Downtime: #{@downtime}s"
  puts msg

  sleep(SLEEP_INTERVAL)
end
